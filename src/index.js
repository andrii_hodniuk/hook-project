import CurrentUserChecker from 'components/currentUserChecker';
import Topbar from 'components/topBar';
import {CurrentUserProvider} from 'contexts/currentUser';

import Routes from 'pages/routes';
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';

const App = () => {
	return (
		<CurrentUserProvider>
			<CurrentUserChecker>
				<Router>
					<Topbar />
					<Routes />
				</Router>
			</CurrentUserChecker>
		</CurrentUserProvider>
	);
};

ReactDOM.render(<App />, document.getElementById('root'));
